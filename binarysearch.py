#Aplikasi Binary Search Aldi Aprilianto

def binarySearch(array, x, low, high):

    # ulangi ketika bertemu tinggi dan rendah
    while low <= high:

        mid = low + (high - low)//2

        if array[mid] == x:
            return mid

        elif array[mid] < x:
            low = mid + 1

        else:
            high = mid - 1

    return -1


array = [2, 7, 3, 4, 5, 1, 9]
x = 4

result = binarySearch(array, x, 0, len(array)-1)

if result != -1:
    print("Elemenya ada  di indeks Ke-" + str(result))
else:
    print("Gak ada!")