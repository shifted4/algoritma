#include <stdio.h>

int main()
{
    int pilihMenu;
    float inputNumber;
    float hasil = 0;
    
    printf("====================================\n");
    printf("Kalkulator Berkelanjutan\n");
    printf("====================================\n\n");
    printf("Masukan Angka : ");
    scanf("%f",&hasil);

    printf("Pilih Menu\n\n ");
    printf("1. Tambah (+)\t ");
    printf("2. Kurang (-)\t ");
    printf("3. Kali (*)\t ");
    printf("4. Bagi (/)\t\n\n ");
    printf("Tekan '0' Untuk Hasil\n\n ");
    
    printf("Pilih Menu : ");
    scanf("%d",&pilihMenu);
    
    
    while(pilihMenu != 0) {
        switch (pilihMenu) {
            case 1: 
            printf("Ditambah(+) \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil + inputNumber;
            break;
            
            case 2: 
            printf("Dikurang(-) \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil - inputNumber;
            break;
            
            case 3: 
            printf("Dikali (*)\n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil * inputNumber;
            break;
            
            case 4: 
            printf("Dibagi (/)\n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil / inputNumber;
            break;
        }
        printf("Pilih Menu : ");
        scanf("%d",&pilihMenu);
    }
    
    printf("============\n");
    printf("hasil: %0.2f", hasil);

    return 0;
}
